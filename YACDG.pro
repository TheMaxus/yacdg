# This file is part of YACDG.
#
#   YACDG is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   YACDG is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with YACDG.  If not, see <https://www.gnu.org/licenses/>.

#-------------------------------------------------
#
# Project created by QtCreator 2018-07-19T15:08:08
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = yacdg
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

SOURCES += \
    src/attributeset.cpp \
    src/jsonfile.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/generatortab.cpp

HEADERS += \
    src/attributeset.h \
    src/jsonfile.h \
    src/mainwindow.h \
    src/generatortab.h
FORMS += \
    src/mainwindow.ui \
    src/generatortab.ui
