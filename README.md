README
======

***Yet Another Character Description Generator*** is a program made with C++ and Qt, that creates random character descriptions for different purposes.

The project is open for suggestions and constructive criticizm is always welcome.

Feel free to contribute to the project!