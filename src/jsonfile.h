/* This file is part of YACDG.
 *
 *   YACDG is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   YACDG is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with YACDG.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef JSONFILE_H
#define JSONFILE_H

#include "attributeset.h"
#include <QJsonDocument>
#include <QString>
#include <QVector>

/**
 * @brief The JSONfile class is used for parsing attributeSets form JSON files
 */
class JSONfile {
public:
    JSONfile(QString);
    QVector<AttributeSet> parseJson();
    bool validateJson();

private:
    QJsonDocument jsonDoc;
    bool openFile(QString);
};

#endif // JSONFILE_H
