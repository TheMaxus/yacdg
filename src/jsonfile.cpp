/* This file is part of YACDG.
 *
 *   YACDG is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   YACDG is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with YACDG.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "jsonfile.h"
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

JSONfile::JSONfile(QString filename) { openFile(filename); }

/**
 * @brief JSONfile::openFile opens JSON file
 * @param filename Filename of a JSON file
 * @return True if file opened successfuly, false otherwise
 */
bool JSONfile::openFile(QString filename) {
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
        return false;

    jsonDoc = QJsonDocument::fromJson(file.readAll());
    return true;
}

/**
 * @brief JSONfile::validateJson checks if JSON file is valid
 *        by checking if returned object is null
 * @return True if JSON file is valid, otherwise false
 */
bool JSONfile::validateJson() { return !jsonDoc.isNull(); }

/**
 * @brief JSONfile::parseJson parses the JSON file
 * @return Returns parsed attributeSet
 */
QVector<AttributeSet> JSONfile::parseJson() {
    QJsonArray json(jsonDoc.array());
    QVector<AttributeSet> output;

    for (const auto x : qAsConst(json)) {
        const QString attributeKey = "attributes";
        const QString nameKey = "name";
        QStringList attributeList = x[attributeKey].toVariant().toStringList();
        QString name = x[nameKey].toString();

        if (attributeList.isEmpty())
            continue; // Skip empty attributeSets

        output.push_back(AttributeSet{attributeList, name});
    }

    return output;
}
