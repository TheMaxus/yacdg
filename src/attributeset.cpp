/* This file is part of YACDG.
 *
 *   YACDG is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   YACDG is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with YACDG.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "attributeset.h"
#include <QRandomGenerator>

/*!
 * \brief AttributeSet::AttributeSet is the default constructor for AttributeSet class
 */
AttributeSet::AttributeSet() : name("") {}

/*!
 * \brief AttributeSet::AttributeSet is an overloaded construcctor for attribute class
 * \param setAttributes QStringList which contains attributes
 * \param setName QString which contains the name of the set
 */
AttributeSet::AttributeSet(const QStringList &setAttributes,
                           const QString &setName)
    : name(setName), attributeList(setAttributes) {}

/**
 * @brief AttributeSet::addAttribute adds an attribute to Attributeset
 * @param string attribute to be added
 */
void AttributeSet::addAttribute(const QString &string) {
    attributeList.push_back(string);
}

/**
 * @brief AttributeSet::addAttribute is an overloaded addAttribute
 *        which can be used to add multiple attributes at the same time
 * @param list List of parameters to add
 */
void AttributeSet::addAttribute(const QStringList &list) {
    attributeList.append(list);
}
/**
 * @brief AttributeSet::getAttributes is a getter for attributelist
 * @return attributeList
 */
QStringList AttributeSet::getAttributes() { return attributeList; }

/**
 * @brief AttributeSet::getRandomAttribute is a method to get random attribute from
 *        attributeSet
 * @return A random attribute from attributeSet
 */
QString AttributeSet::getRandomAttribute() {
    return getAttribute(
                QRandomGenerator::global()->bounded(attributeList.size()));
}

/**
 * @brief AttributeSet::getAttribute is a mathod to get a single attribute
 *        from attributeset
 * @param i index of an attribute
 * @return attribute from attributeSet at index i
 */
QString AttributeSet::getAttribute(int i) { return attributeList.at(i); }

/**
 * @brief AttributeSet::getName is a getter method for name variable
 * @return name
 */
QString AttributeSet::getName() { return name; }
