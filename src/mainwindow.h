/* This file is part of YACDG.
 *
 *   YACDG is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   YACDG is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with YACDG.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "generatortab.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void printResult(const QString &text);

private slots:
    void on_actionAbout_Qt_triggered();
    void on_actionAbout_YACDG_triggered();
    void on_actionLoad_file_triggered();
    void on_actionReset_buttons_triggered();
    void on_actionSave_output_as_triggered();
    void on_actionSave_output_to_file_triggered();
    void on_pushButton_22_clicked();
    void on_resetName_clicked();

private:
    Ui::MainWindow *ui;
    QVector<QSharedPointer<GeneratorTab>> tabs;
    static void writeToFile(const QString &, QString filename);
    void connectButtons();
    void filenameToTitle(QString &);
    void generateAll();
    void loadDefaultValues();
    void loadValues(const QVector<AttributeSet> &);
    void reset();
};

#endif // MAINWINDOW_H
