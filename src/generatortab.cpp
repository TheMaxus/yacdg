/* This file is part of YACDG.
 *
 *   YACDG is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   YACDG is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with YACDG.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "generatortab.h"
#include "ui_generatortab.h"
#include <QRandomGenerator>

/**
 * @brief GeneratorTab::GeneratorTab is the default constructor
 * @param parent parent widget
 * @param attributes attribute set which generator tab uses
 */
GeneratorTab::GeneratorTab(QWidget *parent, const AttributeSet &attributes)
    : QWidget(parent), ui(new Ui::GeneratorTab), attributeSet(attributes) {
    ui->setupUi(this);
    buttons = {ui->pushButton, ui->pushButton_2, ui->pushButton_3};
}

/**
 * @brief GeneratorTab::~GeneratorTab is the Destructor for GeneratorTab class
 */
GeneratorTab::~GeneratorTab() { delete ui; }

/**
 * @brief GeneratorTab::buttonsSetEnabled is a method to enable/disable all buttons
 *        in the widget
 * @param b Boolean value. True to enable, false to disable.
 */
void GeneratorTab::buttonsSetEnabled(bool b) {
    for (auto x : qAsConst(buttons)) {
        x->setEnabled(b);
    }
}

/**
 * @brief GeneratorTab::getName is a getter function for name variable
 * @return name
 */
QString GeneratorTab::getName() { return attributeSet.getName(); }
/**
 * @brief GeneratorTab::getRandomAttribute runs getRandomAttribute method from
 *        attributeset
 * @return A random attribute from attributeSet
 */
QString GeneratorTab::getRandomAttribute() {
    return attributeSet.getRandomAttribute();
}

/**
 * @brief GeneratorTab::reset is a method to reset the widget to a default state
 */
void GeneratorTab::reset() {
    buttonsSetEnabled(true);
    for (auto b : qAsConst(buttons)) {
        b->setText(QString::number(buttons.indexOf(b) + 1));
        b->setStyleSheet("");
    }
}

void GeneratorTab::on_pushButton_clicked() { buttonClicked(ui->pushButton); }

void GeneratorTab::on_pushButton_2_clicked() {
    buttonClicked(ui->pushButton_2);
}

void GeneratorTab::on_pushButton_3_clicked() {
    buttonClicked(ui->pushButton_3);
}

/**
 * @brief GeneratorTab::buttonClicked is a method which is executed when any button in a widget is clicked
 * @param button A button from which the function is called
 */
void GeneratorTab::buttonClicked(QPushButton *button) {
    for (auto b : qAsConst(buttons)) {
        b->setText(getRandomAttribute());
    }
    button->setStyleSheet("font-weight: bold;"
                          "color: #ff0000;");
    buttonsSetEnabled(false);
    emit attributeGenerated(
                QString("%1: %2").arg(attributeSet.getName(), button->text()));
}

/**
 * @brief GeneratorTab::clickRandomButton executes buttonClicked function
 *        for a random button
 */
void GeneratorTab::clickRandomButton() {
    buttonClicked(
                buttons.at(QRandomGenerator::global()->bounded(buttons.size())));
}
