/* This file is part of YACDG.
 *
 *   YACDG is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   YACDG is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with YACDG.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ATTRIBUTESET_H
#define ATTRIBUTESET_H
#include <QString>
#include <QStringList>
#include <QVector>

/**
 * @brief The AttributeSet class
 *
 * Contains an attribute set and a name.
 */
class AttributeSet {
public:
    AttributeSet();
    AttributeSet(const QStringList &, const QString &);

    QString getName();
    void addAttribute(const QString &);
    void addAttribute(const QStringList &);
    QStringList getAttributes();
    QString getAttribute(int);
    QString getRandomAttribute();

private:
    QString name;
    QStringList attributeList;
};

#endif // ATTRIBUTESET_H
