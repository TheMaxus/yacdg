/* This file is part of YACDG.
 *
 *   YACDG is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   YACDG is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with YACDG.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <QDateTime>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QRegularExpression>
#include <QSharedPointer>
#include <QString>
#include <QTextStream>
#include <QVector>

#include "attributeset.h"
#include "jsonfile.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

/**
 * @brief MainWindow::MainWindow is a default constructor for main window
 * @param parent Parent widget
 */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    loadDefaultValues();
}

/**
 * @brief MainWindow::~MainWindow is the default destructor for MainWindow class
 */
MainWindow::~MainWindow() { delete ui; }

/**
 * @brief MainWindow::connectButtons connects signal emmited from a tab to printResult slot
 */
void MainWindow::connectButtons() {
    for (const auto &tab : qAsConst(tabs)) {
        QObject::connect(tab.data(), &GeneratorTab::attributeGenerated, this,
                         &MainWindow::printResult);
    }
}

/**
 * @brief MainWindow::loadDefaultValues loads default attributeSets
 */
void MainWindow::loadDefaultValues() {
    QVector<AttributeSet> defaultAttributes = {
        AttributeSet({"Short", "Medium", "Long"}, "Hair"),
        AttributeSet(
        {"Green", "Blue", "Ginger", "Chestnut", "Dark", "White", "Blond"},
        "Hair color"),
        AttributeSet({"Glasses", "Birthmarks", "Augmentations", "Eye patch",
                      "Bandana", "Aviation Glasses", "Heterochromia"},
        "Features"),
        AttributeSet({"Modern", "18th Century Western", "Medieval Western",
                      "Medieval Eastern"},
        "Clothing style"),
        AttributeSet({"Steampunk", "Cyberpunk", "Normal"}, "Genre")};
    loadValues(defaultAttributes);
}

/**
 * @brief MainWindow::loadValues loads values
 * @param attributes List of attributes to load
 */
void MainWindow::loadValues(const QVector<AttributeSet> &attributes) {
    ui->tabWidget->clear();
    tabs.clear();

    for (const auto &attribute : attributes) {
        QSharedPointer<GeneratorTab> tab =
                QSharedPointer<GeneratorTab>(new GeneratorTab(this, attribute));
        ui->tabWidget->addTab(tab.data(), tab->getName());
        tabs.push_back(tab);
    }
    connectButtons();
}

/**
 * @brief MainWindow::generateAll generates random attributes from each tab
 */
void MainWindow::generateAll() {
    ui->textBrowser->append("");
    for (auto tab : qAsConst(tabs)) {
        tab->reset();
        tab->clickRandomButton();
    }
}

/**
 * @brief MainWindow::reset resets all tabs
 */
void MainWindow::reset() {
    for (auto tab : qAsConst(tabs)) {
        tab->reset();
    }

    ui->textBrowser->clear();
}

void MainWindow::on_actionReset_buttons_triggered() { reset(); }

/**
 * @brief MainWindow::writeToFile writes output to a file
 *        Used to write contents of a text area into a file
 * @param output String to be written int a file
 * @param filename Filename of a file
 */
void MainWindow::writeToFile(const QString &output, QString filename) {
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return;
    }

    QTextStream out(&file);
    out << output << "\n";
    file.close();
}

void MainWindow::on_pushButton_22_clicked() { generateAll(); }

void MainWindow::on_resetName_clicked() { reset(); }

/**
 * @brief MainWindow::on_actionAbout_YACDG_triggered prints info about the program
 */
void MainWindow::on_actionAbout_YACDG_triggered() {
    QSharedPointer<QMessageBox> about =
            QSharedPointer<QMessageBox>(new QMessageBox(this));
    about->setWindowTitle("About YACDG");
    about->setText("<h3>About YACDG</h3>"
                   "<b>YACDG</b> stands for <i>Yet Another Character Description "
                   "Generator</i>");
    about->setInformativeText(
                "<p>This small program is designed to randomly create character "
                "for different purposes: drawing, roleplaying, etc.</p>"
                "<p>Licensed under <a "
                "href=\"https://www.gnu.org/licenses/gpl-3.0.en.html\"> GPLv3</p>"
                "<p> Source code is available on <a "
                "href=\"https://gitlab.com/TheMaxus/yacdg\">GitLab</p>");
    about->exec();
}

void MainWindow::on_actionAbout_Qt_triggered() { QMessageBox::aboutQt(this); }

/**
 * @brief MainWindow::on_actionSave_output_to_file_triggered saves output to a file.
 *        If file already exists appends number to a file.
 */
void MainWindow::on_actionSave_output_to_file_triggered() {
    QString filename = "";
    QString dateTime = QDateTime::currentDateTime().toString();

    filename = QString("lastresult-%1.txt").arg(dateTime);
    writeToFile(ui->textBrowser->toPlainText(), filename);
}

/**
 * @brief MainWindow::printResult prints attribute generated from a tab
 * @param text Text from a tab
 */
void MainWindow::printResult(const QString &text) {
    ui->textBrowser->append(text);
}

/**
 * @brief MainWindow::on_actionSave_output_as_triggered save text area contents into a file.
 */
void MainWindow::on_actionSave_output_as_triggered() {
    QString filename = "";
    filename = QFileDialog::getSaveFileName(
                   this, "Save as...", "./lastresult.txt", "Plain text (*.txt)");
    if (filename != "") {
        writeToFile(ui->textBrowser->toPlainText(), filename);
    }
}

/**
 * @brief MainWindow::on_actionLoad_file_triggered loads JSON file and loads values
 */
void MainWindow::on_actionLoad_file_triggered() {
    QString filename = QFileDialog::getOpenFileName(
                           this, "Load file...", "./default.json", "JSON File (*.json)");
    JSONfile file(filename);
    if (file.validateJson()) {
        loadValues(file.parseJson());
        filenameToTitle(filename);
    } else {
        QMessageBox msgBox;
        msgBox.setWindowTitle("JSON Parsing error");
        msgBox.setText("Invalid or empty file!");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
}

/**
 * @brief MainWindow::filenameToTitle sets the title of a main window according to a file being used
 *        and strips path from the filename
 * @param filename Path to a file
 */
void MainWindow::filenameToTitle(QString &filename) {
    QString jsonName = filename.remove(".json");
    jsonName = jsonName.remove(
                   (QRegularExpression(".+/|.+\\\\"))); /* Remove path before filename */
    this->setWindowTitle(QString("YACDG - %1").arg(jsonName));
}
