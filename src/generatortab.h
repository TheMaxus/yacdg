/* This file is part of YACDG.
 *
 *   YACDG is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   YACDG is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with YACDG.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GENERATORTAB_H
#define GENERATORTAB_H

#include "attributeset.h"
#include <QPushButton>
#include <QWidget>

namespace Ui {
class GeneratorTab;
}

/**
 * @brief The GeneratorTab class contains a widget for each tab
 *        in the main window
 */
class GeneratorTab : public QWidget {
    Q_OBJECT

public:
    explicit GeneratorTab(QWidget *parent = nullptr,
                          const AttributeSet &attributes = AttributeSet());
    ~GeneratorTab();
    void reset();
    QString getName();
    void clickRandomButton();

signals:
    void attributeGenerated(QString text);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();

private:
    QString getRandomAttribute();
    void buttonsSetEnabled(bool);
    Ui::GeneratorTab *ui;
    AttributeSet attributeSet;
    void buttonClicked(QPushButton *);
    QVector<QPushButton *> buttons;
};

#endif // GENERATORTAB_H
